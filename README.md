# WLed - ESP32 - Hardware

[![version](https://img.shields.io/badge/version-0.2-brightgreen.svg)](changelog.md)
[![license](https://img.shields.io/badge/licence-OSHW_v1.0-blue)](licence.md)

***
- Hardware propuesto para el sistema WLed:
https://github.com/Aircoookie/WLED
- Permite el control de Leds:
NeoPixel (WS2812B, WS2811, SK6812) LEDs or also SPI based chipsets like the WS2801 and APA102!
- ESP32 Node MCU (mas velocidad, mejor rendimiento, mas leds)
- Bornes de conexión
- Convertidor de nivel para todas las salidas
- 10 salidas para Leds
- Hasta 800 Leds WS2812 por cada salida
- Salida de Rele de 5V (led Build)
- Entrada para IR
- Entrada para boton
- Pensado para ser fabricado en casa
- Diseño en KiCAD:
https://www.kicad.org
- Firmware en: https://github.com/Aircoookie/WLED

***
## Componentes
**ESP32:**
- https://es.aliexpress.com/item/1005002433798656.html?spm=a2g0o.productlist.0.0.7b9b6eff4GkGXl&algo_pvid=a0de7634-9c3b-4faf-9913-4dde0183dec1&algo_exp_id=a0de7634-9c3b-4faf-9913-4dde0183dec1-17

**Level Converter MH:**
- https://es.aliexpress.com/item/32794304980.html?spm=a2g0o.productlist.0.0.228324869dfrsP&algo_pvid=03d4ae3a-6648-4b0a-b2d0-0b19fcc567c9&algo_exp_id=03d4ae3a-6648-4b0a-b2d0-0b19fcc567c9-0

**Terminals:**
- https://es.aliexpress.com/item/32867190033.html?spm=a2g0o.productlist.0.0.6bff4aa9bbaiEb&algo_pvid=d6807e0e-83b4-4523-ac00-1822097887fc&algo_exp_id=d6807e0e-83b4-4523-ac00-1822097887fc-7

***
## Esquemático
[![Esquematico](https://bitbucket.org/syhs-dev/0080-wled-esp32-hardware/raw/53bbc1605f7bf0735e3be553d59f3fefc1c8f95f/Hardware/Esquematico.png)]()

***
## PCB
[![PCB](https://bitbucket.org/syhs-dev/0080-wled-esp32-hardware/raw/53bbc1605f7bf0735e3be553d59f3fefc1c8f95f/Hardware/PCB.png)]()

***
## Configuracion en WLed
- **Button:** GPIO 27
- **Relay:** GPIO 2
- **IR:** GPIO 14
- **Out 10:** GPIO 25
- **Out 9:** GPIO 33
- **Out 8:** GPIO 18
- **Out 7:** GPIO 19
- **Out 6:** GPIO 22
- **Out 5:** GPIO 23
- **Out 4:** GPIO 6 - CLK
- **Out 3:** GPIO 4
- **Out 2:** GPIO 17
- **Out 1:** GPIO 5

***
## Changelog
### v0.2
- Agregado CLK
### v0.1
- Correccion de pistas
- Correccion de librerias
### v0.0
- Hello World!

***
## **Licenciamiento**

![](https://bitbucket.org/arduino-dmx-512-tester-and-controller/arduino-dmx-512-tester-and-controller-lcd-20x4-mega-hardware/raw/3b0ee0057565b4032ce3c998b0034040be0ef406/Social/Logos/oshw.png)

[Open Source Hardware (OSHW) v1.0](LICENCE.md)

***

## **Autor**

- @daniel3514
- (C) 2021

[![Email](https://bitbucket.org/arduino-dmx-512-tester-and-controller/arduino-dmx-512-tester-and-controller-lcd-20x4-mega-firmware/raw/fa540e928423e9772937151d221e059669b98ebe/Social/Logos/email%2050x50.jpg)](mailto:daniel3514@gmail.com)
[![Facebook](https://bitbucket.org/arduino-dmx-512-tester-and-controller/arduino-dmx-512-tester-and-controller-lcd-20x4-mega-firmware/raw/fa540e928423e9772937151d221e059669b98ebe/Social/Logos/Facebook%2050x50.png)](https://www.facebook.com/daniel.3514)
[![Twitter](https://bitbucket.org/arduino-dmx-512-tester-and-controller/arduino-dmx-512-tester-and-controller-lcd-20x4-mega-firmware/raw/fa540e928423e9772937151d221e059669b98ebe/Social/Logos/Twitter%2050x50.png)](https://twitter.com/daniel3514)

***