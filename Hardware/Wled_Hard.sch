EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "WLed - ESP32 - Hardware"
Date "2021-07-17"
Rev "v0.2"
Comp "@daniel3514"
Comment1 "ID: 0080"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Wled_Hard-rescue:Level_Converter_MH-Healt_Control-rescue-Healt_Control-rescue-Wled_Hard-rescue U4
U 1 1 60EDAB36
P 8750 5000
F 0 "U4" H 9100 5365 50  0000 C CNN
F 1 "Level_Converter_MH" H 9100 5274 50  0000 C CNN
F 2 "Library:level_converter_mh" H 8750 5000 50  0001 C CNN
F 3 "" H 8750 5000 50  0001 C CNN
	1    8750 5000
	-1   0    0    1   
$EndComp
Text GLabel 6950 3500 2    50   Input ~ 0
GND
Text GLabel 8000 4900 0    50   Input ~ 0
GND
Wire Wire Line
	8000 4900 8050 4900
Text GLabel 8000 4800 0    50   Input ~ 0
3.3V
Wire Wire Line
	8000 4800 8050 4800
Text GLabel 4800 5300 0    50   Input ~ 0
5V
Wire Wire Line
	4800 5300 4850 5300
Text GLabel 8800 4800 2    50   Input ~ 0
5V
Wire Wire Line
	8750 4800 8800 4800
Text GLabel 3850 4000 2    50   Input ~ 0
GPIO2
$Comp
L Wled_Hard-rescue:Level_Converter_MH-Healt_Control-rescue-Healt_Control-rescue-Wled_Hard-rescue U3
U 1 1 60EEEDED
P 8750 4050
F 0 "U3" H 9100 4415 50  0000 C CNN
F 1 "Level_Converter_MH" H 9100 4324 50  0000 C CNN
F 2 "Library:level_converter_mh" H 8750 4050 50  0001 C CNN
F 3 "" H 8750 4050 50  0001 C CNN
	1    8750 4050
	-1   0    0    1   
$EndComp
Text GLabel 8000 3950 0    50   Input ~ 0
GND
Wire Wire Line
	8000 3950 8050 3950
Text GLabel 8000 3850 0    50   Input ~ 0
3.3V
Wire Wire Line
	8000 3850 8050 3850
Text GLabel 8800 3850 2    50   Input ~ 0
5V
Wire Wire Line
	8750 3850 8800 3850
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J8
U 1 1 60F0F3AF
P 9350 5100
F 0 "J8" H 9430 5092 50  0000 L CNN
F 1 "Data Out" H 9430 5001 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 9350 5100 50  0001 C CNN
F 3 "" H 9350 5100 50  0001 C CNN
	1    9350 5100
	1    0    0    1   
$EndComp
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J7
U 1 1 60F111E1
P 9350 4700
F 0 "J7" H 9430 4692 50  0000 L CNN
F 1 "Data Out" H 9430 4601 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 9350 4700 50  0001 C CNN
F 3 "" H 9350 4700 50  0001 C CNN
	1    9350 4700
	1    0    0    1   
$EndComp
Wire Wire Line
	8750 4600 9150 4600
Wire Wire Line
	8750 4700 9150 4700
Wire Wire Line
	8750 5000 9150 5000
Wire Wire Line
	8750 5100 9150 5100
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J5
U 1 1 60F17FD4
P 9350 3750
F 0 "J5" H 9430 3742 50  0000 L CNN
F 1 "Data Out" H 9430 3651 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 9350 3750 50  0001 C CNN
F 3 "" H 9350 3750 50  0001 C CNN
	1    9350 3750
	1    0    0    1   
$EndComp
Wire Wire Line
	8750 4050 9150 4050
Wire Wire Line
	8750 4150 9150 4150
Wire Wire Line
	8750 3750 9150 3750
Wire Wire Line
	8750 3650 9150 3650
Wire Wire Line
	8000 3650 8050 3650
Wire Wire Line
	8000 3750 8050 3750
Wire Wire Line
	8000 4600 8050 4600
Wire Wire Line
	8000 4700 8050 4700
Wire Wire Line
	8000 5000 8050 5000
Wire Wire Line
	8000 5100 8050 5100
Wire Wire Line
	3100 4200 3050 4200
Text GLabel 3050 4200 0    50   Input ~ 0
5V
Wire Wire Line
	3850 4200 3800 4200
Text GLabel 4800 3500 0    50   Input ~ 0
3.3V
$Comp
L Wled_Hard-rescue:Level_Converter_MH-Healt_Control-rescue-Healt_Control-rescue-Wled_Hard-rescue U1
U 1 1 60F00BC6
P 3100 4400
F 0 "U1" H 3450 4765 50  0000 C CNN
F 1 "Level_Converter_MH" H 3450 4674 50  0000 C CNN
F 2 "Library:level_converter_mh" H 3100 4400 50  0001 C CNN
F 3 "" H 3100 4400 50  0001 C CNN
	1    3100 4400
	1    0    0    1   
$EndComp
Wire Wire Line
	2700 4900 2650 4900
Wire Wire Line
	2700 5000 2650 5000
Text GLabel 2700 4900 2    50   Input ~ 0
GND
Text GLabel 2700 5000 2    50   Input ~ 0
5V
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J4
U 1 1 60EE2F5D
P 2450 5000
F 0 "J4" H 2530 4992 50  0000 L CNN
F 1 "Power" H 2530 4901 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 2450 5000 50  0001 C CNN
F 3 "" H 2450 5000 50  0001 C CNN
	1    2450 5000
	-1   0    0    1   
$EndComp
Text GLabel 4800 4200 0    50   Input ~ 0
GPIO33
Text GLabel 3850 4500 2    50   Input ~ 0
GPIO33
Text GLabel 4800 4300 0    50   Input ~ 0
GPIO25
Text GLabel 3850 4400 2    50   Input ~ 0
GPIO25
Wire Wire Line
	3800 4400 3850 4400
Wire Wire Line
	3800 4500 3850 4500
Wire Wire Line
	2650 4400 3100 4400
Wire Wire Line
	2650 4500 3100 4500
Text GLabel 3850 4100 2    50   Input ~ 0
GPIO14
Wire Wire Line
	3800 4100 3850 4100
Text GLabel 4800 4600 0    50   Input ~ 0
GPIO14
Wire Wire Line
	2650 4100 3100 4100
Wire Wire Line
	2650 4000 3100 4000
Text GLabel 4800 3700 0    50   Input ~ 0
GPIO27
Text GLabel 2700 3650 2    50   Input ~ 0
GPIO27
Wire Wire Line
	3800 4000 3850 4000
Text GLabel 4800 4800 0    50   Input ~ 0
GND
Wire Wire Line
	4800 4800 4850 4800
Text GLabel 3050 4300 0    50   Input ~ 0
GND
Text GLabel 8800 4900 2    50   Input ~ 0
GND
Text GLabel 8800 3950 2    50   Input ~ 0
GND
Wire Wire Line
	8750 4900 8800 4900
Wire Wire Line
	8750 3950 8800 3950
Wire Wire Line
	3050 4300 3100 4300
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J1
U 1 1 60F515E3
P 2450 3550
F 0 "J1" H 2530 3542 50  0000 L CNN
F 1 "2 Screw" H 2530 3451 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 2450 3550 50  0001 C CNN
F 3 "" H 2450 3550 50  0001 C CNN
	1    2450 3550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2650 3650 2700 3650
Text GLabel 2700 3550 2    50   Input ~ 0
GND
Wire Wire Line
	2650 3550 2700 3550
Text GLabel 6950 4900 2    50   Input ~ 0
GPIO2
Text Notes 9900 5150 0    50   ~ 0
Out 1
Text Notes 9900 5050 0    50   ~ 0
Out 2
Text Notes 9900 4750 0    50   ~ 0
Out 3
Text Notes 9900 4650 0    50   ~ 0
Out 4 - CLK
Text Notes 9900 4200 0    50   ~ 0
Out 5
Text Notes 9900 4100 0    50   ~ 0
Out 6
Text Notes 9900 3800 0    50   ~ 0
Out 7
Text Notes 9900 3700 0    50   ~ 0
Out 8
Text Notes 1700 4550 0    50   ~ 0
Out 9
Text Notes 1650 4450 0    50   ~ 0
Out 10
Wire Wire Line
	8000 4050 8050 4050
Wire Wire Line
	8000 4150 8050 4150
Text Notes 1700 4050 0    50   ~ 0
Relay
Text Notes 1800 4150 0    50   ~ 0
IR
Text Notes 1650 3700 0    50   ~ 0
Button
Text Notes 1750 3600 0    50   ~ 0
GND
Text Notes 1750 4950 0    50   ~ 0
GND
Text Notes 1700 5050 0    50   ~ 0
5 Vcc
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J6
U 1 1 60F177A5
P 9350 4150
F 0 "J6" H 9430 4142 50  0000 L CNN
F 1 "Data Out" H 9430 4051 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 9350 4150 50  0001 C CNN
F 3 "" H 9350 4150 50  0001 C CNN
	1    9350 4150
	1    0    0    1   
$EndComp
$Comp
L Wled_Hard-rescue:ESP32_NodeMCU_38pin U2
U 1 1 60F4E65E
P 5700 4650
F 0 "U2" H 5875 6065 50  0000 C CNN
F 1 "ESP32_NodeMCU_38pin" H 5875 5974 50  0000 C CNN
F 2 "Library:nodemcu_esp32_38pin_no_screws" H 6050 5100 50  0001 C CNN
F 3 "" H 6050 5100 50  0001 C CNN
	1    5700 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3500 4850 3500
Text GLabel 3850 4200 2    50   Input ~ 0
3.3V
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J3
U 1 1 60F2D226
P 2450 4400
F 0 "J3" H 2530 4392 50  0000 L CNN
F 1 "Data Out" H 2530 4301 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 2450 4400 50  0001 C CNN
F 3 "" H 2450 4400 50  0001 C CNN
	1    2450 4400
	-1   0    0    -1  
$EndComp
$Comp
L Wled_Hard-rescue:Screw_Terminal_01x02-Connector-Healt_Control-rescue-Healt_Control-rescue J2
U 1 1 60F30582
P 2450 4000
F 0 "J2" H 2530 3992 50  0000 L CNN
F 1 "2 Screw" H 2530 3901 50  0000 L CNN
F 2 "Library:TerminalBlock_RND_205-00287_1x02_P5.08mm_Horizontal" H 2450 4000 50  0001 C CNN
F 3 "" H 2450 4000 50  0001 C CNN
	1    2450 4000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4800 3700 4850 3700
Wire Wire Line
	6900 4900 6950 4900
Wire Wire Line
	4800 4600 4850 4600
Wire Wire Line
	4800 4200 4850 4200
Wire Wire Line
	4800 4300 4850 4300
Wire Wire Line
	6900 3500 6950 3500
Wire Wire Line
	6950 3700 6900 3700
Text GLabel 6950 3600 2    50   Input ~ 0
GPIO23
Wire Wire Line
	6900 3600 6950 3600
Text GLabel 8000 4150 0    50   Input ~ 0
GPIO23
Text GLabel 6950 3700 2    50   Input ~ 0
GPIO22
Text GLabel 8000 4050 0    50   Input ~ 0
GPIO22
Text GLabel 6950 4200 2    50   Input ~ 0
GPIO19
Wire Wire Line
	6900 4200 6950 4200
Text GLabel 8000 3750 0    50   Input ~ 0
GPIO19
Text GLabel 6950 4300 2    50   Input ~ 0
GPIO18
Wire Wire Line
	6900 4300 6950 4300
Text GLabel 8000 3650 0    50   Input ~ 0
GPIO18
Text GLabel 6950 4400 2    50   Input ~ 0
GPIO5
Wire Wire Line
	6900 4400 6950 4400
Text GLabel 8000 5100 0    50   Input ~ 0
GPIO5
Text GLabel 6950 4500 2    50   Input ~ 0
GPIO17
Wire Wire Line
	6900 4500 6950 4500
Text GLabel 8000 5000 0    50   Input ~ 0
GPIO17
Text GLabel 6950 4700 2    50   Input ~ 0
GPIO4
Wire Wire Line
	6900 4700 6950 4700
Text GLabel 8000 4700 0    50   Input ~ 0
GPIO4
Wire Wire Line
	6900 5300 6950 5300
Text GLabel 8000 4600 0    50   Input ~ 0
GPIO6
Text GLabel 6950 5300 2    50   Input ~ 0
GPIO6
$EndSCHEMATC
